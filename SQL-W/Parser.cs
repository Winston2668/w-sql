﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SQL_W
{
    public class WSqlParser
    {
        private Dictionary<string, string> filesInfo;

        public Dictionary<string, string> GetFileInfo()
        {
            return filesInfo;
        }

        public WSqlParser()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.*", SearchOption.AllDirectories)
                             .Where(file => new string[] { ".wsql" }.Contains(Path.GetExtension(file)))
                             .ToList();
            filesInfo = new Dictionary<string, string>();
            foreach (var file in files)
            {
                filesInfo.Add(Path.GetFileName(file) ?? "", File.ReadAllText(file));
            }
        }

        public void Parse()
        {
            foreach (var file in filesInfo)
            {
                var script = file.Value;
            }

        }
    }

    public abstract class ProcessNode
    {
        public int ProcessIndex { get; set; }
        public List<ProcessUnit> ProcessUnits { get; set; }
        public abstract string Process();
        public string Result { get; set; }

        public void ValidateSyntax()
        {
            var patternList = new List<Regex>();
            patternList.Add(new Regex(@"(?i)select(?-i)"));
            ProcessUnits.ForEach(x =>
            {
                //Syntax check rule.
                var valid = false;
                patternList.ForEach(pattern =>
                {
                    if (pattern.IsMatch(x.Body))
                        valid = true;
                });
                if (!valid)
                    throw new SqlWSyntaxException("Syntax error in this part: " + x.Body);
            });
        }

        public void InitProcessUnits(string script)
        {
            var units = script.Split(';').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)).ToList();
            var regex = new Regex(@"@\w+:");
            var index = 0;
            ProcessUnits = new List<ProcessUnit>();
            units.ForEach(unit =>
            {
                var matched = regex.Match(unit);
                if (matched.Success)
                {
                    ProcessUnits.Add(new ProcessUnit()
                    {
                        Head = matched.Value.Substring(0, matched.Value.Length - 1),
                        Body = unit.Substring(matched.Length),
                        Index = index++
                    });
                }
                else
                {
                    throw new SqlWSyntaxException("Syntaxt error in: " + unit);
                }
            });
        }

        protected ProcessNode(string script)
        {
            Result = "";
            try
            {
                InitProcessUnits(script);
                ValidateSyntax();
            }
            catch (SqlWSyntaxException ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }

    public class ProcessUnit
    {
        public string BaseScript { get; set; }
        public string Head { get; set; }
        public string Body { get; set; }
        public int Index { get; set; }

        public ProcessUnit()
        {

        }

        public ProcessUnit(ProcessUnit p)
        {
            BaseScript = p.BaseScript;
            Head = p.Head;
            Body = p.Body;
            Index = p.Index;
        }
    }
}
