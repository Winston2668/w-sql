﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_W
{
    public class SqlWSyntaxException : Exception
    {
        public SqlWSyntaxException(string message) : base(message)
        {

        }
    }
}
