﻿using System.Text.RegularExpressions;

namespace SQL_W
{
    public class FeatureSelect : ProcessNode
    {
        private class LexicalContext
        {
            public string PrevScript { get; set; }
            public string NextScript { get; set; }
            public string CurrentWord { get; set; }

            public LexicalContext()
            {
                PrevScript = "";
                NextScript = "";
                CurrentWord = "";
            }
        }

        private LexicalContext _context;

        private void InitContext(ProcessUnit unit)
        {
            Regex regex = new Regex(@"\$\w+\s*=>\s*\(.+\)");
            _context = new LexicalContext();
            if (regex.IsMatch(unit.Body))
            {
                var matched = regex.Match(unit.Body);
                _context.CurrentWord = matched.Value;
                _context.PrevScript = unit.Body.Substring(0, matched.Index).Trim();
                _context.NextScript = unit.Body.Substring(matched.Index + matched.Length).Trim();
            }
            else
            {
                _context.PrevScript = unit.Body;
            }
        }

        private void ProcessOnContext()
        {
            if (_context.NextScript == "")
                return;

            var regex = new Regex(@"\(.+\)");
            var newColumns = regex.Match(_context.CurrentWord).Value;
            newColumns = newColumns.Substring(1, newColumns.Length - 2);
            regex = new Regex(@"\w+");
            var newTableName = regex.Match(_context.CurrentWord);
            _context.PrevScript = string.Format("SELECT {0} FROM ({1}) {2} ", newColumns, _context.PrevScript, newTableName);

            regex = new Regex(@"\$\w+\s*=>\s*\(.+\)");
            var matched = regex.Match(_context.NextScript);
            if (!matched.Success)
            {
                _context.PrevScript += _context.NextScript;
                _context.NextScript = "";
            }
            else
            {
                var newPrev = _context.NextScript.Substring(0, matched.Index).Trim();
                var newNext = _context.NextScript.Substring(matched.Index + matched.Length).Trim();
                _context.CurrentWord = matched.Value;
                _context.PrevScript += newPrev;
                _context.NextScript = newNext;
            }
            ProcessOnContext();
        }

        public FeatureSelect(string script) : base(script)
        {
        }

        public override string Process()
        {
            var resultSql = "";
            foreach (var unit in ProcessUnits)
            {
                InitContext(unit);
                ProcessOnContext();
                resultSql += string.Format("{0}: {1};", unit.Head, _context.PrevScript);
            }
            Result = resultSql;
            return resultSql;
        }

    }
}
