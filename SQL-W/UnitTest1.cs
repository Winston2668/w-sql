﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SQL_W
{
    [TestClass]
    public class UnitTestWSqlParser
    {
        [TestMethod]
        public void TestReadFile()
        {
            var fileInfo = new WSqlParser().GetFileInfo();          
            Assert.IsTrue(fileInfo.ContainsKey("sqltest.wsql"));
            Assert.IsTrue(fileInfo["sqltest.wsql"].Length > 0);
        }

        [TestMethod]
        public void TestFeatureSelect()
        {
            var fileInfo = new WSqlParser().GetFileInfo();
            var script = fileInfo["sqltest.wsql"];
            var selectFeature = new FeatureSelect(script);
            var result = selectFeature.Process();
        }

        [TestMethod]
        public void TestFeatureBlock()
        {
            var fileInfo = new WSqlParser().GetFileInfo();
            var script = fileInfo["sqltest.wsql"];
            var selectFeature = new FeatureSelect(script);
            var result = selectFeature.Process();
            var blockFeature = new FeatureBlock(result);
            var r = blockFeature.Process();

            var blockFeature1 = new FeatureBlock(r);
            var s = "";
            foreach (var unit in blockFeature1.ProcessUnits)
            {
                s += unit.Body;
            }
            File.WriteAllText(@"D:\temp.sql", s);

            //C:\Python27\python.exe

            string fileName = @"D:\formatter.py";

            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(@"C:\Python27\python.exe", fileName)
            {
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };
            p.Start();

            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            Console.WriteLine(output);
        }

    }
}
