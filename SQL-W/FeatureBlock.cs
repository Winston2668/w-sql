﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SQL_W
{
    class FeatureBlock : ProcessNode
    {
        public FeatureBlock(string script) : base(script)
        {
        }

        public override string Process()
        {
            var evaluated = new Dictionary<string, string>();
            while (true)
            {
                var beforeEvaluated = new Dictionary<string, string>(evaluated);
                var beforeUnits = ProcessUnits.Select(x => new ProcessUnit(x)).ToList();
                foreach (var unit in ProcessUnits)
                {
                    if (Evaluated(unit))
                    {
                        if (!evaluated.ContainsKey(unit.Head))
                        {
                            evaluated.Add(unit.Head, unit.Body);
                        }
                        else
                        {
                            evaluated[unit.Head] = unit.Body;
                        }
                    }
                    else
                    {
                        Regex regex = new Regex(@"@\w+");
                        var matched = regex.Matches(unit.Body);
                        foreach (Match match in matched)
                        {
                            if (evaluated.ContainsKey(match.Value))
                            {
                                unit.Body = unit.Body.Replace(match.Value,
                                    $"({evaluated[match.Value]}) {match.Value.Substring(1)}");
                            }
                        }
                    }
                }
                if (ProcessUnits.Count(x => !Evaluated(x)) == 0)
                    break;
                if (beforeEvaluated.OrderBy(x => x.Key).SequenceEqual(evaluated.OrderBy(x => x.Key)) && beforeUnits.SequenceEqual(ProcessUnits))
                {
                    foreach (var unit in ProcessUnits)
                    {
                        if(!Evaluated(unit))
                            throw new SqlWSyntaxException($"You code can't be evaluated. In block {unit.Head}: \n {unit.Body}");
                    }
                    
                }
            }

            var resultSql = "";
            foreach (var unit in ProcessUnits)
            {
                resultSql += unit.Head + ":" + unit.Body + ";\n\n";
            }
            Result = resultSql;
            return resultSql;
        }

        private bool ContainsBlock(ProcessUnit unit)
        {
            Regex regex = new Regex(@"@\w+");
            return regex.IsMatch(unit.Body);
        }

        private bool Evaluated(ProcessUnit unit)
        {
            return !ContainsBlock(unit);
        }
    }
}
